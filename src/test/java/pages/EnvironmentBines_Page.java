package pages;

public class EnvironmentBines_Page extends Base_Page{

    public static String environment = "src/test/resources/query/Environment_Bines.xml";

    /*************************
     ***** CREATE ORIGEN *****
     *************************/

    public static String createBIN_LOCAL() {
        String create = fetchQuery(environment).getProperty("createBIN_LOCAL");
        return create;
    }

    public static String createCLIENTE_EMISOR() {
        String create = fetchQuery(environment).getProperty("createCLIENTE_EMISOR");
        return create;
    }

    /****************************
     ***** DROPTABLE ORIGEN *****
     ****************************/

    public static String dropBIN_LOCAL() {
        String create = fetchQuery(environment).getProperty("dropBIN_LOCAL");
        return create;
    }

    public static String dropCLIENTE_EMISOR() {
        String create = fetchQuery(environment).getProperty("dropCLIENTE_EMISOR");
        return create;
    }

    /**********************
     ***** DELETE RAW *****
     **********************/

    public static String deleteBIN_LOCAL() {
        String create = fetchQuery(environment).getProperty("deleteBIN_LOCAL");
        return create;
    }

    public static String deleteCLIENTE_EMISOR() {
        String create = fetchQuery(environment).getProperty("deleteCLIENTE_EMISOR");
        return create;
    }

    /**********************
     ***** DELETE MCP *****
     **********************/

    public static String deletePROCESO_METRICA_REL_BIN_LOCAL() {
        String create = fetchQuery(environment).getProperty("deletePROCESO_METRICA_REL_BIN_LOCAL");
        return create;
    }

    public static String deletePROCESO_METRICA_REL_CLIENTE_EMISOR() {
        String create = fetchQuery(environment).getProperty("deletePROCESO_METRICA_REL_CLIENTE_EMISOR");
        return create;
    }

    public static String deleteEJECUCION_PROCESO() {
        String create = fetchQuery(environment).getProperty("deleteEJECUCION_PROCESO");
        return create;
    }

    /**********************
     ***** DELETE LOG *****
     **********************/

    public static String logBIN_LOCAL() {
        String create = fetchQuery(environment).getProperty("logBIN_LOCAL");
        return create;
    }

    public static String logCLIENTE_EMISOR() {
        String create = fetchQuery(environment).getProperty("logCLIENTE_EMISOR");
        return create;
    }

    /***********************
     ***** DELETE HDFS *****
     ***********************/

    public static String hdfsBIN_LOCAL() {
        String create = fetchQuery(environment).getProperty("hdfsBIN_LOCAL");
        return create;
    }

    public static String hdfsCLIENTE_EMISOR() {
        String create = fetchQuery(environment).getProperty("hdfsCLIENTE_EMISOR");
        return create;
    }
}

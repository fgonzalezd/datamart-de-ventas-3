package pages;

public class QueryTableMstrgeo_Page extends Base_Page{

    public static String queryXML = "src/test/resources/query/QueryTable_Mstrgeo.xml";

    /*********************
     ***** QUERY RAW *****
     *********************/

    public static String queryCOMUNA() {
        String query = fetchQuery(queryXML).getProperty("queryCOMUNA");
        return query;
    }

    public static String queryCOUNT_COMUNA() {
        String query = fetchQuery(queryXML).getProperty("queryCOUNT_COMUNA");
        return query;
    }

    /*********************
     ***** QUERY MCP *****
     *********************/

    public static String queryEJECUCION_PROCESO_COMUNA() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COMUNA");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_COMUNA() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_COMUNA");
        return query;
    }
}

package pages;

public class QueryTableBpos_Page extends Base_Page{

    public static String queryXML = "src/test/resources/query/QueryTable_Bpos.xml";

    /*********************
     ***** QUERY RAW *****
     *********************/

    public static String queryEQUIPOXEST() {
        String query = fetchQuery(queryXML).getProperty("queryEQUIPOXEST");
        return query;
    }

    public static String queryCOUNT_EQUIPOXEST() {
        String query = fetchQuery(queryXML).getProperty("queryCOUNT_EQUIPOXEST");
        return query;
    }

    /*********************
     ***** QUERY MCP *****
     *********************/

    public static String queryEJECUCION_PROCESO_EQUIPOXEST() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_EQUIPOXEST");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_EQUIPOXEST() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_EQUIPOXEST");
        return query;
    }
}

package properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesInit {

    public static Properties fetchProperties(){
        Properties properties = new Properties();
        try {
            FileInputStream fileInputStream = new FileInputStream("src/test/resources/config/config.properties");
            properties.load(fileInputStream);
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

    /**
     * Metodos que retornan un parametro de configuracion
     */

    /* ORACLE */
    public static String OracleDriver() {
        String param = fetchProperties().getProperty("oracle.driver");
        return param;
    }

    public static String OracleURL() {
        String param = fetchProperties().getProperty("oracle.url");
        return param;
    }

    public static String OracleUser() {
        String param = fetchProperties().getProperty("oracle.user");
        return param;
    }

    public static String OraclePass() {
        String param = fetchProperties().getProperty("oracle.pass");
        return param;
    }

    public static String OracleScheme() {
        String param = fetchProperties().getProperty("oracle.scheme");
        return param;
    }

    /* HIVE */
    public static String HiveDriver() {
        String param = fetchProperties().getProperty("hive.driver");
        return param;
    }

    public static String HiveURL() {
        String param = fetchProperties().getProperty("hive.url");
        return param;
    }

    public static String UserRAW() {
        String param = fetchProperties().getProperty("user.raw");
        return param;
    }

    public static String PassRAW() {
        String param = fetchProperties().getProperty("pass.raw");
        return param;
    }

    public static String UserCUR() {
        String param = fetchProperties().getProperty("user.cur");
        return param;
    }

    public static String PassCUR() {
        String param = fetchProperties().getProperty("pass.cur");
        return param;
    }

    public static String UserLOB() {
        String param = fetchProperties().getProperty("user.lob");
        return param;
    }

    public static String PassLOB() {
        String param = fetchProperties().getProperty("pass.lob");
        return param;
    }

    /* SCHEMA */
    public static String HiveSchemaStgTxs() {
        String param = fetchProperties().getProperty("hive.schema.stg.txs");
        return param;
    }

    public static String HiveSchemaMCP() {
        String param = fetchProperties().getProperty("hive.schema.mcp");
        return param;
    }

    public static String HiveSchemaRawArc() {
        String param = fetchProperties().getProperty("hive.schema.raw.arc");
        return param;
    }

    public static String HiveSchemaRawParcom() {
        String param = fetchProperties().getProperty("hive.schema.raw.parcom");
        return param;
    }

    public static String HiveSchemaRawTxs() {
        String param = fetchProperties().getProperty("hive.schema.raw.txs");
        return param;
    }

    public static String HiveSchemaRawTab() {
        String param = fetchProperties().getProperty("hive.schema.raw.tab");
        return param;
    }

    public static String HiveSchemaRawBin() {
        String param = fetchProperties().getProperty("hive.schema.raw.bin");
        return param;
    }

    public static String HiveSchemaRawCtss() {
        String param = fetchProperties().getProperty("hive.schema.raw.ctss");
        return param;
    }

    public static String HiveSchemaRawMstrgeo() {
        String param = fetchProperties().getProperty("hive.schema.raw.mstrgeo");
        return param;
    }

    public static String HiveSchemaRawBpos() {
        String param = fetchProperties().getProperty("hive.schema.raw.bpos");
        return param;
    }

    public static String HiveSchemaRawSiebel() {
        String param = fetchProperties().getProperty("hive.schema.raw.siebel");
        return param;
    }

    public static String HiveSchemaCurTiempo() {
        String param = fetchProperties().getProperty("hive.schema.cur.tiempo");
        return param;
    }

    public static String HiveSchemaCurTxProcesadas() {
        String param = fetchProperties().getProperty("hive.schema.cur.txproc");
        return param;
    }

    /* HOST QA */
    public static String SshHost() {
        String param = fetchProperties().getProperty("ssh.host");
        return param;
    }

    public static int SshPort() {
        int param = Integer.parseInt(fetchProperties().getProperty("ssh.port"));
        return param;
    }

    public static String SshUser() {
        String param = fetchProperties().getProperty("ssh.user");
        return param;
    }

    public static String SshPass() {
        String param = fetchProperties().getProperty("ssh.pass");
        return param;
    }

    /* HOST SLAVE */
    public static String SshUrlSlave() {
        String param = fetchProperties().getProperty("ssh.url.slave");
        return param;
    }

    public static String SshHostSlave() {
        String param = fetchProperties().getProperty("ssh.host.slave");
        return param;
    }

    public static int SshPortSlave() {
        int param = Integer.parseInt(fetchProperties().getProperty("ssh.port.slave"));
        return param;
    }

    public static String SshUserSlave() {
        String param = fetchProperties().getProperty("ssh.user.slave");
        return param;
    }

    public static String SshPassSlave() {
        String param = fetchProperties().getProperty("ssh.pass.slave");
        return param;
    }
}
@sPartyPer_N7

Feature: S_PARTY_PER N7 - No existe data nueva para extraer

  Scenario: Ambientacion para S_PARTY_PER "No existe data nueva para extraer"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para S_PARTY_PER caso N_siete
    When se debe insertar el dataset para las tablas de Oracle para S_PARTY_PER caso N_siete
    And se debe insertar el dataset para las tablas de hive para S_PARTY_PER caso N_siete
    And se crea el archivo de secuencia en el hdfs para S_PARTY_PER caso N_siete
    Then se debe ejecutar el proceso de extraccion para S_PARTY_PER caso N_siete

  Scenario: Validacion para tabla S_PARTY_PER "No existe data nueva para extraer"
    When hago una consulta a la tabla del RAW para tabla S_PARTY_PER caso N_siete
    And hago una consulta a la tabla del MCP para tabla S_PARTY_PER caso N_siete
    Then valido que la insercion a la tabla RAW sea correcta para tabla S_PARTY_PER caso N_siete
    And valido que el registro de insercion en el MCP fue correcto para tabla S_PARTY_PER caso N_siete

  Scenario: Validacion de archivos de frecuencia S_PARTY_PER "No existe data nueva para extraer"
    When hago una consulta al archivo lock para S_PARTY_PER caso N_siete
    And hago una consulta al archivo de frecuencia para S_PARTY_PER caso N_siete
    Then valido que el archivo lock haya sido eliminado correctamente para S_PARTY_PER caso N_siete
    And valido que el archivo de secuencia tenga la ultima secuencia para S_PARTY_PER caso N_siete

  Scenario: Validacion de archivos en hdfs para S_PARTY_PER "No existe data nueva para extraer"
    When hago una consulta a los archivos en TO_IMPORT para S_PARTY_PER caso N_siete
    And hago una consulta a los archivos en TO_TRANSFER para S_PARTY_PER caso N_siete
    And hago una consulta a los archivos en LOADING para S_PARTY_PER caso N_siete
    Then valido que la cantidad de archivos en el hdfs sea correcta para S_PARTY_PER caso N_siete

  Scenario: Validacion de logs S_PARTY_PER "No existe data nueva para extraer"
    When hago una consulta a los logs generados en RAW para S_PARTY_PER caso N_siete
    Then valido que la cantidad de logs generados sea correcto para S_PARTY_PER caso N_siete

  Scenario: Limpieza ambientacion para S_PARTY_PER "No existe data nueva para extraer"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para S_PARTY_PER caso N_siete
@direcciones_N2

Feature: Direcciones N2 - La fecha de los datos ya fue ejecutada OK

  Scenario: Ambientacion para Direcciones "La fecha de los datos ya fue ejecutada OK"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Direcciones caso N_dos
    When se debe insertar el dataset para las tablas de Oracle para Direcciones caso N_dos
    And se debe insertar el dataset para las tablas de hive para Direcciones caso N_dos
    Then se debe ejecutar el proceso de extraccion para Direcciones caso N_dos

  Scenario: Validacion para tabla Direcciones "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a la tabla del RAW para tabla Direcciones caso N_dos
    And hago una consulta a la tabla del MCP para tabla Direcciones caso N_dos
    Then valido que la insercion a la tabla RAW sea correcta para tabla Direcciones caso N_dos
    And valido que el registro de insercion en el MCP fue correcto para tabla Direcciones caso N_dos

  Scenario: Validacion de archivos en hdfs para Direcciones "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a los archivos en TO_IMPORT para Direcciones caso N_dos
    And hago una consulta a los archivos en TO_TRANSFER para Direcciones caso N_dos
    And hago una consulta a los archivos en LOADING para Direcciones caso N_dos
    Then valido que la cantidad de archivos en el hdfs sea correcta para Direcciones caso N_dos

  Scenario: Validacion de logs Direcciones "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a los logs generados en RAW para Direcciones caso N_dos
    Then valido que la cantidad de logs generados sea correcto para Direcciones caso N_dos

  Scenario: Limpieza ambientacion para Direcciones "La fecha de los datos ya fue ejecutada OK"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Direcciones caso N_dos
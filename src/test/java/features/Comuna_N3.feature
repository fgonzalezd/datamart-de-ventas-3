@comuna_N3

Feature: Comuna N3 - La fecha de los datos ya fue ejecutada con ERROR

  Scenario: Ambientacion para Comuna "La fecha de los datos ya fue ejecutada con ERROR"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Comuna caso N_tres
    When se debe insertar el dataset para las tablas de Oracle para Comuna caso N_tres
    And se debe insertar el dataset para las tablas de hive para Comuna caso N_tres
    Then se debe ejecutar el proceso de extraccion para Comuna caso N_tres

  Scenario: Validacion para tabla Comuna "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a la tabla del RAW para tabla Comuna caso N_tres
    And hago una consulta a la tabla del MCP para tabla Comuna caso N_tres
    Then valido que la insercion a la tabla RAW sea correcta para tabla Comuna caso N_tres
    And valido que el registro de insercion en el MCP fue correcto para tabla Comuna caso N_tres

  Scenario: Validacion de archivos en hdfs para Comuna "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a los archivos en TO_IMPORT para Comuna caso N_tres
    And hago una consulta a los archivos en TO_TRANSFER para Comuna caso N_tres
    And hago una consulta a los archivos en LOADING para Comuna caso N_tres
    Then valido que la cantidad de archivos en el hdfs sea correcta para Comuna caso N_tres

  Scenario: Validacion de logs Comuna "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a los logs generados en RAW para Comuna caso N_tres
    Then valido que la cantidad de logs generados sea correcto para Comuna caso N_tres

  Scenario: Limpieza ambientacion para Comuna "La fecha de los datos ya fue ejecutada con ERROR"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Comuna caso N_tres
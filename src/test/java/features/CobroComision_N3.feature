@cobroComision_N3

Feature: Cobro_Comision_Retencion N3 - La fecha de los datos ya fue ejecutada con ERROR

  Scenario: Ambientacion para "La fecha de los datos ya fue ejecutada con ERROR"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Cobro_Comision_Retencion caso N_tres
    When se debe insertar el dataset para las tablas de Oracle para Cobro_Comision_Retencion caso N_tres
    And se debe insertar el dataset para las tablas de hive para Cobro_Comision_Retencion caso N_tres
    Then se debe ejecutar el proceso de extraccion con una fecha del 8/7/2019 para Cobro_Comision_Retencion caso N_tres

  Scenario: Validacion para tabla Cobro_Comision_Retencion "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a la tabla del RAW para tabla Cobro_Comision_Retencion caso N_tres
    And hago una consulta a la tabla del MCP para tabla Cobro_Comision_Retencion caso N_tres
    Then valido que la insercion a la tabla RAW sea correcta para tabla Cobro_Comision_Retencion caso N_tres
    And valido que el registro de insercion en el MCP fue correcto para tabla Cobro_Comision_Retencion caso N_tres

  Scenario: Validacion de logs Cobro_Comision_Retencion "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a los logs generados en RAW para Cobro_Comision_Retencion caso N_tres
    Then valido que la cantidad de logs generados sea correcto para Cobro_Comision_Retencion caso N_tres

  Scenario: Limpieza ambientacion para "La fecha de los datos ya fue ejecutada con ERROR"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Cobro_Comision_Retencion caso N_tres
@fechaCalendario_N5

Feature: Fecha_Calendario N5 - Metrica activa sin regla

  Scenario: Ambientacion para "Metrica activa sin regla"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Fecha_Calendario caso N_cinco
    When se debe insertar el dataset para las tablas de RAW para Fecha_Calendario caso N_cinco
    And se debe insertar el dataset para las tablas de CUR para Fecha_Calendario caso N_cinco
    Then se debe ejecutar el proceso de Fecha_Calendario caso N_cinco

  Scenario: Validacion para tabla Fecha_Calendario "Metrica activa sin regla"
    When hago una consulta a la tabla del CUR para tabla Fecha_Calendario caso N_cinco
    And hago una consulta a la tabla del MCP para tabla Fecha_Calendario caso N_cinco
    Then valido que la insercion a la tabla CUR sea correcta para tabla Fecha_Calendario caso N_cinco
    And valido que el registro de insercion en el MCP fue correcto para tabla Fecha_Calendario caso N_cinco

  Scenario: Validacion de logs Fecha_Calendario "Metrica activa sin regla"
    When hago una consulta a los logs generados en RAW para Fecha_Calendario caso N_cinco
    Then valido que la cantidad de logs generados sea correcto para Fecha_Calendario caso N_cinco

  Scenario: Limpieza ambientacion para "Metrica activa sin regla"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Fecha_Calendario caso N_cinco
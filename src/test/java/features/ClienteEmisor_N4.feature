@clienteEmisor_N4

Feature: Cliente_Emisor N4 - Metrica deshabilitada

  Scenario: Ambientacion para Cliente_Emisor "Metrica deshabilitada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Cliente_Emisor caso N_cuatro
    When se debe insertar el dataset para las tablas de Oracle para Cliente_Emisor caso N_cuatro
    And se debe insertar el dataset para las tablas de hive para Cliente_Emisor caso N_cuatro
    Then se debe ejecutar el proceso de extraccion para Cliente_Emisor caso N_cuatro

  Scenario: Validacion para tabla Cliente_Emisor "Metrica deshabilitada"
    When hago una consulta a la tabla del RAW para tabla Cliente_Emisor caso N_cuatro
    And hago una consulta a la tabla del MCP para tabla Cliente_Emisor caso N_cuatro
    Then valido que la insercion a la tabla RAW sea correcta para tabla Cliente_Emisor caso N_cuatro
    And valido que el registro de insercion en el MCP fue correcto para tabla Cliente_Emisor caso N_cuatro

  Scenario: Validacion de archivos en hdfs para Cliente_Emisor "Metrica deshabilitada"
    When hago una consulta a los archivos en TO_IMPORT para Cliente_Emisor caso N_cuatro
    And hago una consulta a los archivos en TO_TRANSFER para Cliente_Emisor caso N_cuatro
    And hago una consulta a los archivos en LOADING para Cliente_Emisor caso N_cuatro
    Then valido que la cantidad de archivos en el hdfs sea correcta para Cliente_Emisor caso N_cuatro

  Scenario: Validacion de logs Cliente_Emisor "Metrica deshabilitada"
    When hago una consulta a los logs generados en RAW para Cliente_Emisor caso N_cuatro
    Then valido que la cantidad de logs generados sea correcto para Cliente_Emisor caso N_cuatro

  Scenario: Limpieza ambientacion para Cliente_Emisor "Metrica deshabilitada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Cliente_Emisor caso N_cuatro
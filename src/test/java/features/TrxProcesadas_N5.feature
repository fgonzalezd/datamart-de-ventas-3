@trxProcesadas_N5

Feature: Transacciones_Procesadas N5 - Metrica activa sin regla

  Scenario: Ambientacion para "Metrica activa sin regla"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Transacciones_Procesadas caso N_cinco
    When se debe insertar el dataset para las tablas de RAW para Transacciones_Procesadas caso N_cinco
    And se debe insertar el dataset para las tablas de CUR para Transacciones_Procesadas caso N_cinco
    Then se debe ejecutar el proceso de extraccion con una fecha del 9/7/2019 para Transacciones_Procesadas caso N_cinco

  Scenario: Validacion para tabla Transacciones_Procesadas "Metrica activa sin regla"
    When hago una consulta a la tabla del CUR para tabla Transacciones_Procesadas caso N_cinco
    And hago una consulta a la tabla del MCP para tabla Transacciones_Procesadas caso N_cinco
    Then valido que la insercion a la tabla CUR sea correcta para tabla Transacciones_Procesadas caso N_cinco
    And valido que el registro de insercion en el MCP fue correcto para tabla Transacciones_Procesadas caso N_cinco

  Scenario: Validacion de logs Transacciones_Procesadas "Metrica activa sin regla"
    When hago una consulta a los logs generados en RAW para Transacciones_Procesadas caso N_cinco
    Then valido que la cantidad de logs generados sea correcto para Transacciones_Procesadas caso N_cinco

  Scenario: Limpieza ambientacion para "Metrica activa sin regla"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Transacciones_Procesadas caso N_cinco
@equipoxest_N1

Feature: Equipoxest N1 - La fecha de los datos no ha sido ejecutada

  Scenario: Ambientacion para Equipoxest "La fecha de los datos no ha sido ejecutada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Equipoxest caso N_uno
    When se debe insertar el dataset para las tablas de Oracle para Equipoxest caso N_uno
    And se debe insertar el dataset para las tablas de hive para Equipoxest caso N_uno
    Then se debe ejecutar el proceso de extraccion para Equipoxest caso N_uno

  Scenario: Validacion para tabla Equipoxest "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a la tabla del RAW para tabla Equipoxest caso N_uno
    And hago una consulta a la tabla del MCP para tabla Equipoxest caso N_uno
    Then valido que la insercion a la tabla RAW sea correcta para tabla Equipoxest caso N_uno
    And valido que el registro de insercion en el MCP fue correcto para tabla Equipoxest caso N_uno

  Scenario: Validacion de archivos en hdfs para Equipoxest "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a los archivos en TO_IMPORT para Equipoxest caso N_uno
    And hago una consulta a los archivos en TO_TRANSFER para Equipoxest caso N_uno
    And hago una consulta a los archivos en LOADING para Equipoxest caso N_uno
    Then valido que la cantidad de archivos en el hdfs sea correcta para Equipoxest caso N_uno

  Scenario: Validacion de logs Equipoxest "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a los logs generados en RAW para Equipoxest caso N_uno
    Then valido que la cantidad de logs generados sea correcto para Equipoxest caso N_uno

  Scenario: Limpieza ambientacion para Equipoxest "La fecha de los datos no ha sido ejecutada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Equipoxest caso N_uno
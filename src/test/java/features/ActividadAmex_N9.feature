@actividadAmex_N9

Feature: Actividad_Amex N9 - Ingreso de fecha erronea en el parametro

  Scenario: Ejecucion del proceso extraccion
    When Se ejecuta el proceso de extraccion con una fecha erronea para Actividad_Amex caso N_Nueve
    Then se valida que el proceso de extraccion se haya terminado con error Actividad_Amex caso N_Nueve

  Scenario: Ejecucion del proceso STG
    When Se ejecuta el proceso de carga STG con una fecha erronea para Actividad_Amex caso N_Nueve
    Then se valida que el proceso de carga STG se haya terminado con error Actividad_Amex caso N_Nueve

  Scenario: Ejecucion del proceso RAW
    When Se ejecuta el proceso de carga RAW con una fecha erronea para Actividad_Amex caso N_Nueve
    Then se valida que el proceso de carga RAW se haya terminado con error Actividad_Amex caso N_Nueve
@trxProcesadas_N4

Feature: Transacciones_Procesadas N4 - Metrica deshabilitada

  Scenario: Ambientacion para "Metrica deshabilitada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Transacciones_Procesadas caso N_cuatro
    When se debe insertar el dataset para las tablas de RAW para Transacciones_Procesadas caso N_cuatro
    And se debe insertar el dataset para las tablas de CUR para Transacciones_Procesadas caso N_cuatro
    Then se debe ejecutar el proceso de extraccion con una fecha del 10/7/2019 para Transacciones_Procesadas caso N_cuatro

  Scenario: Validacion para tabla Transacciones_Procesadas "Metrica deshabilitada"
    When hago una consulta a la tabla del CUR para tabla Transacciones_Procesadas caso N_cuatro
    And hago una consulta a la tabla del MCP para tabla Transacciones_Procesadas caso N_cuatro
    Then valido que la insercion a la tabla CUR sea correcta para tabla Transacciones_Procesadas caso N_cuatro
    And valido que el registro de insercion en el MCP fue correcto para tabla Transacciones_Procesadas caso N_cuatro

  Scenario: Validacion de logs Transacciones_Procesadas "Metrica deshabilitada"
    When hago una consulta a los logs generados en RAW para Transacciones_Procesadas caso N_cuatro
    Then valido que la cantidad de logs generados sea correcto para Transacciones_Procesadas caso N_cuatro

  Scenario: Limpieza ambientacion para "Metrica deshabilitada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Transacciones_Procesadas caso N_cuatro
@binLocal_N1

Feature: Bin_Local N1 - La fecha de los datos no ha sido ejecutada

  Scenario: Ambientacion para Bin_Local "La fecha de los datos no ha sido ejecutada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Bin_Local caso N_uno
    When se debe insertar el dataset para las tablas de Oracle para Bin_Local caso N_uno
    And se debe insertar el dataset para las tablas de hive para Bin_Local caso N_uno
    Then se debe ejecutar el proceso de extraccion para Bin_Local caso N_uno

  Scenario: Validacion para tabla Bin_Local "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a la tabla del RAW para tabla Bin_Local caso N_uno
    And hago una consulta a la tabla del MCP para tabla Bin_Local caso N_uno
    Then valido que la insercion a la tabla RAW sea correcta para tabla Bin_Local caso N_uno
    And valido que el registro de insercion en el MCP fue correcto para tabla Bin_Local caso N_uno

  Scenario: Validacion de archivos en hdfs para Bin_Local "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a los archivos en TO_IMPORT para Bin_Local caso N_uno
    And hago una consulta a los archivos en TO_TRANSFER para Bin_Local caso N_uno
    And hago una consulta a los archivos en LOADING para Bin_Local caso N_uno
    Then valido que la cantidad de archivos en el hdfs sea correcta para Bin_Local caso N_uno

  Scenario: Validacion de logs Bin_Local "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a los logs generados en RAW para Bin_Local caso N_uno
    Then valido que la cantidad de logs generados sea correcto para Bin_Local caso N_uno

  Scenario: Limpieza ambientacion para Bin_Local "La fecha de los datos no ha sido ejecutada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Bin_Local caso N_uno